import { createClient } from "react-fetching-library";
import cache from "./cache";

import { requestHostInterceptor, requestAuthInterceptor } from "./interceptors";

const API_URL = process.env.REACT_APP_API_URL || "http://localhost:4000/api";

export const client = createClient({
  requestInterceptors: [
    requestHostInterceptor(API_URL),
    requestAuthInterceptor()
  ]
});

export const clientCache = createClient({
  requestInterceptors: [
    requestHostInterceptor(API_URL),
    requestAuthInterceptor()
  ],
  cacheProvider: cache
});
