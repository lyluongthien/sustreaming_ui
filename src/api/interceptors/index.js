import AuthService from 'services/auth';

export const requestHostInterceptor = host => () => async action => {
  return {
    method: 'GET',
    ...action,
    endpoint: action.endpoint.startsWith('http') ? action.endpoint : `${host}${action.endpoint}`,
  };
};

export const requestAuthInterceptor = () => () => async action => {
  const token = AuthService.getToken();
  if (token) {
    action.headers = {
      Authorization: `Bearer ${token}`,
      ...(action.headers || {}),
    };
  }
  return {
    ...action,
  };
};
