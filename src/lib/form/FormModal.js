import React from "react";
import { useHistory } from "react-router-dom";
import { Button, Modal, ModalHeader, ModalBody, ModalFooter } from "reactstrap";
import { Form } from "lib/form";

const FormModal = ({
  children,
  title = "",
  onComplete,
  onCancel,
  btnSubmitLabel = "Submit",
  redirect = true,
  noButtons = false,
  ...props
}) => {
  let history = useHistory();

  return (
    <Form
      onSubmitSuccess={async (result, passedProps) => {
        if (onComplete) {
          await onComplete(result, passedProps);
        }
        return redirect && history.goBack();
      }}
      {...props}
    >
      {formProps => {
        const toggle = () => !formProps.isSubmitting && history.goBack();

        return (
          <Modal isOpen toggle={toggle}>
            <ModalHeader toggle={toggle}>{title}</ModalHeader>
            <ModalBody>
              <fieldset disabled={formProps.isSubmitting}>
                {children(formProps)}
              </fieldset>
            </ModalBody>
            {!noButtons ? (
              <ModalFooter style={{ padding: "0.25rem" }}>
                <Button
                  color="light"
                  onClick={() => (onCancel ? onCancel() : toggle())}
                  disabled={formProps.isSubmitting}
                >
                  Cancel
                </Button>
                <Button
                  type="button"
                  color="primary"
                  onClick={formProps.submitForm}
                  disabled={formProps.isSubmitting}
                >
                  {formProps.isSubmitting ? (
                    <i className="fas fa-spinner fa-spin" />
                  ) : (
                    <span>{btnSubmitLabel}</span>
                  )}
                </Button>
              </ModalFooter>
            ) : null}
          </Modal>
        );
      }}
    </Form>
  );
};

export default FormModal;
