import React from "react";
import { useField, useFormikContext } from "formik";
import {
  FormGroup,
  CustomInput,
  Label,
  FormFeedback,
  FormText
} from "reactstrap";

const RadioButtonsField = ({
  label,
  helperText,
  options = [],
  onChange,
  ...props
}) => {
  const { setFieldValue } = useFormikContext();
  const [field, meta] = useField(props);

  const handleChange = event => {
    const { value: targetValue } = event.target;
    if (onChange) {
      onChange(event);
    }
    return setFieldValue(field.name, targetValue);
  };

  return (
    <FormGroup>
      {label && <Label for={props.id || props.name}>{label}</Label>}
      <div className="wrapper-radio-buttons">
        {Array.from(options).map((option, index) => (
          <CustomInput
            key={option.key || index}
            type="radio"
            id={`${field.name}-${option.key || index}`}
            name={field.name}
            label={option.label}
            value={option.value}
            onChange={handleChange}
          />
        ))}
      </div>
      {meta.touched && meta.error ? (
        <FormFeedback>{meta.error}</FormFeedback>
      ) : null}
      {helperText && <FormText>{helperText}</FormText>}
    </FormGroup>
  );
};

export default RadioButtonsField;
