## SuMeet - WebRTC react Web Application

Web Application to transfer video and audio data peer to peer.

To run this project in a dev environtment you need:

- docker.io
- docker-compose

And this application depends of server app:

- https://gitlab.com/victorgt3/sustreaming_server/-/tree/devel

So, you need to start the server app and after you can start UI app if you execute:

- docker-compose up